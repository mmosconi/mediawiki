FROM php:7.0-apache

ENV VERSION="1.27.1"

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
        curl tar \
    && docker-php-ext-install -j$(nproc) iconv mcrypt \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd

RUN docker-php-ext-install -j$(nproc) mysqli

WORKDIR /var/www/html
RUN curl -O https://releases.wikimedia.org/mediawiki/1.27/mediawiki-${VERSION}.tar.gz
RUN tar xvzf mediawiki-$VERSION.tar.gz
RUN rm mediawiki-$VERSION.tar.gz
RUN mv mediawiki-$VERSION/* ./

EXPOSE 80
